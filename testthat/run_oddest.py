#!/usr/bin/python
import subprocess
import time
from multiprocessing import Pool
seed=[1]
lambda0=[0.2,0.1,0.05,0.02,0.01,0.005]
orz=[1,2,5,1.189,1.495]
#orz=[1.189,1.495]
trial=[10]
popu=[1000]
match=[2,3]
distr=['norm,0,1','bern,0.1','bern,0.2','bern,0.5']
filename="EBO_L%d_R%d_T%d_P%d_M%d_D%s_S%d.log"
cmdline="./oddest.R -l %f -r %f -t %d -p %d -m %d -d %s %d >%s 2>&1"
cmds=[]
for l in lambda0:
  for r in orz:
    for t in trial:
      for p in popu:
        for m in match:
          for d in distr:
            for s in seed:
              #lambda0, oddsRatio, nTrials, nPatientsPerTrial, matchingFrequency, simDistribution, seed
              tmp_filename=filename % (int(l*1000),int(r*1000),t,p,m,d,s)
              tmp_cmdfile=open("%s.run" % (tmp_filename), "w")
              tmp_cmdline=cmdline % (l,r,t,p,m,d,s,tmp_filename)
              print >>tmp_cmdfile, tmp_cmdline
              cmds.append(tmp_cmdline)

def exec_cmd(cmd):
  start_time=time.time()
  message=subprocess.Popen( cmd, shell=True ).communicate()
  end_time=time.time()
  print cmd, message[0], message[1], "time cost %d" % (end_time-start_time)

p = Pool(20)
p.map(exec_cmd, cmds)
#find failed simulations
#(for file in `ls *.log`; do pf=${file%.log}; ncsv=`ls $pf*csv | wc -l`; if [[ ! ncsv -eq 3 ]]; then echo $file; fi; done) >failed.sims
#rerun failed simulations
#for sim in `cat failed.sims`; do cat $sim.run; nohup sh $sim.run& done;
print "total", len(cmds), "simulations done"
