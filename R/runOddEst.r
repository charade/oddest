#' Run Odds Estimation
#'
#' @param lambda0 A numeric for true lambda0/beta0, i.e., logit(p/(1-p)) = lambda0 + lambda1 * z
#' @param oddsRatio A numeric for true odds ratio, i.e., oddsRatio=(p1/(1-p1))/(p2/(1-p2)) 
#' @param nTrials A integer for number of trials
#' @param nPatientsPerTrial A integer for number of patients per trial
#' @param matchingFrequency A integer for frequency matching ratio, i.e. (#controls+#cases)/(#cases)
#' @param simDistribution A string for assumed distribution, e.g. Norm(0,1) = norm,0,1; Bernoulli(0.5) = bern,0.5; realDataFile = real,STRABIS,C1,bern,mepeds_strabis.csv
#' @param outPrefix A string for prefix for output file, e.g. none = EBO_L($lambda0*1000)_O($oddsRatio*1000)_T($nTrials)_P($nPatientsPerTrial)_M($matchingFrequency)_D($simDistribution)_S($randomSeed)
#' @param randomSeed A integer for random number generator seed
#' @param debug_file A string for debug_file name if debug is enabled
#' @param output A boolean for if to create output files
#' @return oddEst A list of log odds estimates based to methodology described in our paper
#' @examples
#'
#' @export
runOddEst = function(lambda0 = lambda0, 
                     oddsRatio = oddsRatio, 
                     nTrials = nTrials, 
                     nPatientsPerTrial = nPatientsPerTrial,
                     matchingFrequency = matchingFrequency,
                     simDistribution = simDistribution,
                     outPrefix = outPrefix,
                     randomSeed = randomSeed,
                     debug_file = debug_file,
                     output = TRUE
) {
  
  ### assign z values based on simDistribution
  simZ=NULL
  if(grepl("norm",simDistribution)) simZ=c(-2,-1,0,1,2)
  if(grepl("bern",simDistribution)) simZ=c(0,1)
  
  #common procedures
  coxph_fail_count=0
  svyglm_fail_count=0
  logit_fail_count=0
  
  columnNames = c('pid','trial','z','d','rand') #pid - patient id, trial - trial id, z - exposure i.e. x, d - disease status, rand - random number to compare with p_i to assign disease status
  nColumns=length(columnNames)
  
  if(grepl("real",simDistribution)) {
    ### this part loads real cohort data
    message("---parsing real data ", simDistribution)
    realInfo = strsplit(simDistribution,",")[[1]]
    dataFile = realInfo[[5]]
    response = realInfo[[2]]
    variate = realInfo[[3]]
    realData = read.table(dataFile, sep=",", header=T)
    missing = is.na(realData[[variate]]) | is.na(realData[[response]])
    realData = realData[!missing,]
    if(grepl("norm",simDistribution)) {
      realData[["original"]] = realData[[variate]]
      realData[[variate]] = (realData[[variate]]-mean(realData[[variate]],na.rm=T))/sd(realData[[variate]], na.rm=T)
    }
    glmfit = glm( realData[[response]] ~ realData[[variate]], family=binomial(link='logit'))
    lambda0 = exp(glmfit$coefficients[1])
    oddsRatio = exp(glmfit$coefficients[2])
    nPatientsPerTrial = nrow(realData)
    cohort_data = rep.int(0, nTrials*nPatientsPerTrial*nColumns)
    dim(cohort_data)<- c(nTrials*nPatientsPerTrial, nColumns)
    cohort_data[,1] = rep(1:nPatientsPerTrial, nTrials)
    cohort_data[,2] = sapply(1:nTrials, rep, nPatientsPerTrial)
    cohort_data[,3] = rep(realData[[variate]], nTrials)
    cohort_data[,4] = rep(realData[[response]], nTrials)
    cohort_data[,5] = runif(nTrials*nPatientsPerTrial)
    outfile=paste(dataFile,paste("T=",nTrials,"S=",randomSeed,"D=",response,"Z=",variate,sep=""),"txt",sep=".")
  } else {
    ### this part simulates cohort data
    message("---start simulating ", simDistribution)
    cohort_data = rep.int(0, nTrials*nPatientsPerTrial*nColumns)
    dim(cohort_data)<- c(nTrials*nPatientsPerTrial, nColumns)
    for (t in seq(1,nTrials)){
      for (p in seq(1,nPatientsPerTrial)){
        id = nPatientsPerTrial*(t-1)+p
        z_i = oddest::simRandom(simDistribution)
        lambda_i = lambda0*oddsRatio**z_i
        p_i = lambda_i/(1+lambda_i)
        d_i = runif(1) < p_i
        rand_i = runif(1)
        cohort_data[id,] = c(id, t, z_i, d_i, rand_i)
      }
    } #simulating various exposure
    if(outPrefix=="none") {
      outfile=paste("EBO","_L", as.integer(lambda0*1000),
                    "_R", as.integer(oddsRatio*1000),
                    "_T", as.integer(nTrials),
                    "_P", as.integer(nPatientsPerTrial),
                    "_M", as.integer(matchingFrequency),
                    "_D", simDistribution,
                    "_S", randomSeed, ".txt", sep="")
    } else {
      outfile=paste(outPrefix,"_S",randomSeed,".txt",sep="")
    }
  }
  cohort = data.frame(cohort_data)
  colnames(cohort) = columnNames
  cohort = cohort[with(cohort,order(trial, -d, rand)),]
  message("cohort simulated")
  
  ###this part deals with frequency matching
  j=0
  fm_data = rep(0,nTrials*nPatientsPerTrial*5)
  dim(fm_data) = c(nTrials*nPatientsPerTrial, 5)
  setinfo_data = rep.int(0, nTrials*4)
  dim(setinfo_data) = c(nTrials, 4)
  for (i in seq(1,nTrials*nPatientsPerTrial)){
    if(i %% nPatientsPerTrial == 1) {
      cn=0 #case recruit
      rn=0 #normal recruit
    }
    rn=rn+1
    if(cohort[i,]$d==1){
      cn=cn+1
      j=j+1
      fm_data[j,1]=cohort[i,]$pid
      fm_data[j,2]=cohort[i,]$trial
      fm_data[j,3]=cohort[i,]$d
      fm_data[j,4]=cohort[i,]$z
      fm_data[j,5]=2
    }
    if(i %% nPatientsPerTrial ==0) {
      fmsize=cn*matchingFrequency
      setinfo_data[cohort[i,]$trial,1]=cohort[i,]$trial
      setinfo_data[cohort[i,]$trial,2]=cn
      setinfo_data[cohort[i,]$trial,3]=min(fmsize, nPatientsPerTrial)
      setinfo_data[cohort[i,]$trial,4]=rn
    }
    if(cohort[i,]$d==0 && rn <= cn*matchingFrequency){
      j=j+1
      fm_data[j,1]=cohort[i,]$pid
      fm_data[j,2]=cohort[i,]$trial
      fm_data[j,3]=cohort[i,]$d
      fm_data[j,4]=cohort[i,]$z
      fm_data[j,5]=2
    }
  }
  fm = data.frame(fm_data[1:j,])
  colnames(fm) = c('pid','trial','d','z','exit')
  setinfo = data.frame(setinfo_data)
  colnames(setinfo) = c('trial','ncases','fmsize','n')
  fm = fm[with(fm,order(trial,-d, z)),]
  message("frequency matched")
  
  ### this is a to format case control dataset based on fm
  trial_size = stats::aggregate(pid ~ trial, fm, length)$pid  #number of selected patients per trial
  case_number = stats::aggregate(d ~ trial, fm, sum)$d        #number of cases per trial
  max_trial_size = max(trial_size)                   #size of largest trial
  max_case_number = max(case_number)                 #max number of cases per trail   
  trial_end=c(0, cumsum(trial_size))                 #index points to the where the (i-1)-th trial ends in the combined data frame
  npid = dim(fm)[1]                                  #total number of patients
  pars = c(4)                                        #?
  npars = length(pars)                               # = 1
  weight = 1                                         #?
  case_cont_data = matrix(rep(0,length(trial_size)*(1+max_trial_size*(1+npars)))) # this creates a vector of length: nTrials * (1 + max_trail_size * (1+npars) )
  dim(case_cont_data) = c(length(trial_size), 1+max_trial_size*(1+npars)) # this creates a matrix of nTrials * (1+max_trial_size*(1+npars))
  ti=2 #for dummy
  j=0
  for (i in seq(1,npid)){
    if(i==trial_end[ti-1]+1){ #first      
      v = rep.int(NA, max_trial_size*npars)
      w = rep.int(0, max_trial_size)
      all_control = FALSE
      if(fm[i,]$d == 0) { 
        all_control=TRUE
        next
      }
    } 
    if(all_control) next #if all control, skip trial
    vs=(i-trial_end[ti-1])*npars-(npars-1)
    ve=(i-trial_end[ti-1])*npars
    v[vs:ve]=as.numeric(fm[i,][pars])
    w[i-trial_end[ti-1]]=weight
    #print(vs:ve)
    #print(as.numeric(fm[i,][pars]))
    if(trial_end[ti]==i){#last
      #print(ti)
      ti = ti+1
      j=j+1
      case_cont_data[j,]=c(fm[i,]$trial,v,w)
    }
  }
  case_cont = case_cont_data[1:j, , drop=FALSE]
  # if(is.null(dim(case_cont_data[1:j,]))) {
  #   case_cont = data.frame(t(case_cont_data[1:j,]))
  # } else {
  #   case_cont = case_cont_data[1:j,]
  # }
  colnames(case_cont)=c('trial',paste('v',seq(1,max_trial_size*npars),sep=''),
                        paste('w',seq(1,max_trial_size),sep=''))
  message("formatted case_cont") # matrix of nTrials rows 
  
  ### this does the survival estimates
  ### logistic regression
  colnames(case_cont)[1] = "trial"  #name the first column of case_cont, required 
  beta1=oddest::survivalEstimate2(fm, oddsRatio, trial_size, case_number, useTrueBeta=T)
  beta2=oddest::survivalEstimate2(fm, oddsRatio, trial_size, case_number, useTrueBeta=F)
  betas=data.frame(cbind(beta1,matrix(beta2[,-c(1,4,5)],nrow=dim(beta2)[1])))
  colnames(betas)=c("trial","truebhat","truevbhat","trial_size","case_number","bhat","vbhat")
  coxph_fail_count=sum(is.na(betas[["bhat"]]))
  ctemp=merge(betas, case_cont, by='trial')
  fset=merge(setinfo, ctemp, by='trial')
  message("merged to fset")
  
  #NOTE, na might be caused by too small number of cases
  bodds1=oddest::calc_bo(fset, max_trial_size, npar=1, vi=11, useTrueBeta=T) #true beta
  bodds2=oddest::calc_bo(fset, max_trial_size, npar=1, vi=11, useTrueBeta=F)
  gset=merge(bodds2, merge(bodds1, fset, by='trial'), by='trial')
  message("merged to gset")
  ###
  # in gest
  # case_log_odds: alpha0_hat using case set only
  # baseline_odds: lambda0_hat 
  # log_bline_odds: alpha0_hat using case control set
  ### 
  
  lodds1 = oddest::calc_lo(gset, max_trial_size, npar=1, zi=17, useTrueBeta=T)
  lodds2 = oddest::calc_lo(gset, max_trial_size, npar=1, zi=17, useTrueBeta=F)
  hset = merge(lodds2, merge(lodds1, gset, by='trial'), by='trial')
  message("created hset")
  
  #if(debug) { save.image(debug_file) }
  
  #NEW
  #per trial estimates
  #method-wise difference: see rbe and coe2 below
  #norm: z=c(-2,-1,0,1,2)
  #bern: z=c(0,1)
  #      abzhat = logodds+bhat*z
  #      vabzhat (see below in rbe and coe2 estimators)
  #      abzhat_cu = abzhat + 1.96 * sqrt(vabzhat)
  #      abzhat_cl = abzhat - 1.96 * sqrt(vabzhat)
  #      phat = 1 - 1/(1+exp(abzhat))
  #      phat_cu =  1 - 1/(1+exp(abzhat_cu))
  #      phat_cl =  1 - 1/(1+exp(abzhat_cl))
  
  #all below are nTrial by length(simZ) matricies
  simZnames = paste("Z",simZ,sep="")
  
  coe2_abzhat = hset[["case_log_odds"]] + outer(hset[["bhat"]],simZ); colnames(coe2_abzhat) = paste("coe2_abzhat",simZnames,sep="_")
  rbe_abzhat = hset[["log_bline_odds"]] + outer(hset[["bhat"]],simZ); colnames(rbe_abzhat) = paste("rbe_abzhat",simZnames,sep="_")  
  coe2_vabzhat = hset[["varacases"]] + outer(hset[["vbhat"]],simZ*simZ); colnames(coe2_vabzhat) = paste("coe2_vabzhat",simZnames,sep="_")
  rbe_vabzhat = hset[["varatilde"]] + outer(hset[["vbhat"]],simZ*simZ); colnames(rbe_vabzhat) = paste("rbe_vabzhat",simZnames,sep="_")
  coe2_abzhat_cu = coe2_abzhat + 1.96 * sqrt(coe2_vabzhat); colnames(coe2_abzhat_cu) = paste("coe2_abzhat_cu",simZnames,sep="_")              
  rbe_abzhat_cu = rbe_abzhat + 1.96 * sqrt(rbe_vabzhat); colnames(rbe_abzhat_cu) = paste("rbe_abzhat_cu",simZnames,sep="_")
  coe2_abzhat_cl = coe2_abzhat - 1.96 * sqrt(coe2_vabzhat); colnames(coe2_abzhat_cl) = paste("coe2_abzhat_cl",simZnames,sep="_")
  rbe_abzhat_cl = rbe_abzhat - 1.96 * sqrt(rbe_vabzhat); colnames(rbe_abzhat_cu) = paste("rbe_abzhat_cu",simZnames,sep="_")                  
  coe2_phat = 1 - 1/(1+exp(coe2_abzhat)); colnames(coe2_phat) = paste("coe2_phat",simZnames,sep="_")
  rbe_phat = 1 - 1/(1+exp(rbe_abzhat)); colnames(rbe_phat) = paste("rbe_phat",simZnames,sep="_") 
  coe2_phat_cu =  1 - 1/(1+exp(coe2_abzhat_cu)); colnames(coe2_phat_cu) = paste("coe2_phat_cu",simZnames,sep="_") 
  rbe_phat_cu =  1 - 1/(1+exp(rbe_abzhat_cu)); colnames(rbe_phat_cu) = paste("rbe_phat_cu",simZnames,sep="_")
  coe2_phat_cl =  1 - 1/(1+exp(coe2_abzhat_cl)); colnames(coe2_phat_cl) = paste("coe2_phat_cl",simZnames,sep="_") 
  rbe_phat_cl =  1 - 1/(1+exp(rbe_abzhat_cl)); colnames(rbe_phat_cl) = paste("rbe_phat_cl",simZnames,sep="_") 
  
  hset = cbind(hset, coe2_abzhat, coe2_abzhat_cl, coe2_abzhat_cu, coe2_vabzhat, coe2_phat, coe2_phat_cl, coe2_phat_cu,
               rbe_abzhat, rbe_abzhat_cl, rbe_abzhat_cu, rbe_vabzhat, rbe_phat, rbe_phat_cl, rbe_phat_cu)
  hset[["true_alpha"]] = log(lambda0)
  message("mergeed hset")
  
  sset = cbind(trial_size=hset[["trial_size"]], ncases=hset[["ncases"]],
               alpha=hset[["true_alpha"]], beta=hset[["truebhat"]], beta_est=hset[["bhat"]], varbeta_est=hset[["vbhat"]], 
               coe2_alpha_est=hset[["case_log_odds"]], coe2_varalpha_est=hset[["varacases"]],
               coe2_abzhat, coe2_vabzhat, coe2_abzhat_cl, coe2_abzhat_cu, coe2_phat, coe2_phat_cl, coe2_phat_cu,
               rbe_alpha_est=hset[["log_bline_odds"]], rbe_varalpha_est=hset[["varatilde"]],
               rbe_abzhat, rbe_vabzhat, rbe_abzhat_cl, rbe_abzhat_cu, rbe_phat, rbe_phat_cl, rbe_phat_cu)
  sset = as.data.frame(sset)
  message("mergeed sset")
  
  #      save tables of trail-wise estimates, i.e. hset and sset
  if(output) {
    hsetfile = paste(outfile,"hset","csv",sep=".")
    write.table(hset, hsetfile, sep=",", row.names = F, col.names = T, quote = F)
    ssetfile = paste(outfile,"sset","csv",sep=".")
    write.table(sset, ssetfile, sep=",", row.names = F, col.names = T, quote = F)
  }
  
  #across trial estimates columns
  #      method, truealpha, alphahat, valphahat, evalpha, truebeta, betahat, vbetahat
  #      coe,    
  #      rbe,
  #      mean_phat = mean(phat)
  #      var_phat = var(phat)
  #      mean_phat_cu = mean_phat + 1.96 * sqrt(var_phat)
  #      mean_phat_cl = mean_phat - 1.96 * sqrt(var_phat)
  #      output mean_phat, var_phat, mean_phat_cu, mean_phat_cl
  #      output means of (truebhat, bhat, vbhat)
  
  coe2_mean_phat = colMeans(coe2_phat, na.rm=T); names(coe2_mean_phat)=simZnames
  coe2_var_phat = diag(var(coe2_phat, na.rm=T)); names(coe2_var_phat)=simZnames
  rbe_mean_phat = colMeans(rbe_phat, na.rm=T); names(rbe_mean_phat)=simZnames
  rbe_var_phat = diag(var(rbe_phat, na.rm=T)); names(rbe_var_phat)=simZnames
  coe2_mean_phat_cu = coe2_mean_phat + 1.96 * sqrt(coe2_var_phat); names(coe2_mean_phat_cu)=simZnames
  coe2_mean_phat_cl = pmax(0, coe2_mean_phat - 1.96 * sqrt(coe2_var_phat)); names(coe2_mean_phat_cl)=simZnames
  rbe_mean_phat_cu = rbe_mean_phat + 1.96 * sqrt(rbe_var_phat); names(rbe_mean_phat_cu)=simZnames
  rbe_mean_phat_cl = pmax(0, rbe_mean_phat - 1.96 * sqrt(rbe_var_phat)); names(rbe_mean_phat_cl)=simZnames
  true_p = matrix(colMeans(1 - 1/(1+exp(sset[["alpha"]] + outer(sset[["beta"]], simZ))), na.rm=T),ncol=length(simZ)); colnames(true_p) = simZnames
  
  tset=as.data.frame(list(method=c("coe2","rbe")))
  tset=cbind(tset, 
             alpha=mean(sset[["alpha"]], na.rm=T),
             alpha_est=c(mean(sset[["coe2_alpha_est"]], na.rm=T), mean(sset[["rbe_alpha_est"]], na.rm=T)),
             varalpha_est=c(mean(sset[["coe2_varalpha_est"]], na.rm=T), mean(sset[["rbe_varalpha_est"]], na.rm=T)),
             beta=mean(sset[["beta"]], na.rm=T),
             beta_est=mean(sset[["beta_est"]], na.rm=T),
             varbeta_est=mean(sset[["varbeta_est"]], na.rm=T),
             p=true_p,
             mean_phat=rbind(coe2_mean_phat, rbe_mean_phat),
             var_phat=rbind(coe2_var_phat, rbe_var_phat),
             mean_phat_cl=rbind(coe2_mean_phat_cl, rbe_mean_phat_cl),
             mean_phat_cu=rbind(coe2_mean_phat_cu, rbe_mean_phat_cu)
             # mean_phat_z0=rbind(coe2_mean_phat, rbe_mean_phat)[,1],
             # var_phat_z0=rbind(coe2_var_phat, rbe_var_phat)[,1],
             # mean_phat_cl_z0=rbind(coe2_mean_phat_cl, rbe_mean_phat_cl)[,1],
             # mean_phat_cu_z0=rbind(coe2_mean_phat_cu, rbe_mean_phat_cu)[,1],
             # p_z1=true_p[,2],
             # mean_phat_z1=rbind(coe2_mean_phat, rbe_mean_phat)[,2],
             # var_phat_z1=rbind(coe2_var_phat, rbe_var_phat)[,2],
             # mean_phat_cl_z1=rbind(coe2_mean_phat_cl, rbe_mean_phat_cl)[,2],
             # mean_phat_cu_z1=rbind(coe2_mean_phat_cu, rbe_mean_phat_cu)[,2]
  )
  message("mergeed tset")
  
  if(output) {
    tsetfile = paste(outfile,"tset","csv",sep=".")
    write.table(tset, tsetfile, sep=",", row.names = F, col.names = T, quote = F)
    message("find results in {", hsetfile, ssetfile, tsetfile, "} files")
  }
  message(paste("---", "#fails - logit:", coxph_fail_count))
  
  oddEst = list(hset=hset, sset=sset, tset=tset, outfile=outfile)
  return(oddEst)
}
