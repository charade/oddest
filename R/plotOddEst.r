#' Plot Odds Estimation
#'
#' @param oddEst A list of log odds estimates based to methodology described in our paper
#' @return oddEstPlot A handle of oddEstPlot figure
#' 
#' @examples
#'
#' @export
plotOddEst = function( 
  oddEst = oddEst
  , histCol = c("coe2_alpha_est", "rbe_alpha_est")
  , trueCol = "alpha"
  , output = TRUE) {
  
  nPlots = length(histCol)
  par(mfrow=c(nPlots,1))
  
  for( i in seq.int(nPlots)) {
    trueData = oddEst$sset[[trueCol]][1]
    histData = oddEst$sset[[histCol[i]]]
    hist( histData, col="gray", xlab="", main=histCol[i], probability = TRUE )
    abline( v = trueData, col = "red", lwd=3 )
    curve( dnorm(x, mean=mean(histData), sd=sqrt(var(histData))), add=TRUE, col="blue", lwd=3, lty="dashed" )
    abline( v = mean(histData), col = "blue", lwd=3 )
    limits = par("usr")
    text( x = limits[1] + 0.8*(limits[2]-limits[1]), y = 0.8*(limits[4]-limits[3]), 
        labels = paste("True=",as.character(round(trueData,4))), col = "red", cex=2 )
    text( x = limits[1] + 0.8*(limits[2]-limits[1]), y = 0.9*(limits[4]-limits[3]), 
        labels = paste("Est=", as.character(round(mean(histData),4))), col = "blue", cex=2 )
  }
}