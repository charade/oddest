#' Calculate baseline odds
#' 
#' @param fset A dataframe containing frequency matched case control data of multiple trials. Each row is a trial 
#' @param max_trial_size Maximum number of patients per trial, presented as vi:(vi+max_trial_size*2-1) columns
#' @param npar Number of covariates
#' @param vi First index of patient's column
#' @return odds_set A dataframe containing estimated c('trial','case_log_odds','baseline_odds','log_bline_odds') for each trial
#' @examples
#' 
#' @export

calc_bo <- function(fset, max_trial_size, npar=1, vi=11, useTrueBeta=T){
  case_log_odds = matrix(rep(0,dim(fset)[1]),ncol=dim(fset)[1])
  baseline_odds = matrix(rep(0,dim(fset)[1]),ncol=dim(fset)[1])
  log_bl_odds = matrix(rep(0,dim(fset)[1]),ncol=dim(fset)[1])
  valid_trial = matrix(rep(0,dim(fset)[1]),ncol=dim(fset)[1])
  j=0
  for(i in seq(1,dim(fset)[1])){
    if(useTrueBeta) {
      bhat = fset[i,]$truebhat
    } else {
      bhat = fset[i,]$bhat
    }
    cnum = fset[i,]$ncases
    tnum = fset[i,]$fmsize
    snum = fset[i,]$n
    j=j+1
    if(cnum<3) {
      case_log_odds[1,j] = NA
      baseline_odds[1,j] = NA
      log_bl_odds[1,j] = NA
      valid_trial[1,j] = fset[i,]$trial
      next
    }
    zt = matrix(as.numeric(fset[i,vi:(vi+max_trial_size*npar-1)]), nrow=npar, ncol=max_trial_size, byrow=FALSE)
    #zt[is.na(zt)] = mean(zt, na.rm=T) #now handles missing data during import
    #print(paste(bhat, cnum, tnum, snum, zt))
    phi = as.numeric(exp(zt[1,]*bhat))
    sumphiinv = sum(1/phi[1:cnum])
    #print(paste(phi,sumphiinv))
    #NOTE: this could overflow when summation is too big
    
    #b0a = rep(Rmpfr::mpfr(0,64),max_trial_size)
    #b0b = rep(Rmpfr::mpfr(0,64),max_trial_size)
    b0a = rep(0,max_trial_size)
    b0b = rep(0,max_trial_size)
    b0a[1] = phi[1]
    for(k in seq(2,tnum)){
     b0b[1] = b0a[1] + phi[k]
     for(l in seq(2,min(k,cnum))){ #break down if only one case
       b0b[l] = b0a[l] + b0a[l-1]*phi[k]    #this step gonna overflow
       #print(paste(b0b[l],b0a[l],b0a[l-1],phi[k],b0a[l] + b0a[l-1]*phi[k]))
     }
     for(l in seq(1,min(k,cnum)))
       b0a[l] = b0b[l]
    }
    b0a = ifelse(is.finite(b0a),b0a,.Machine[["double.xmax"]])
    b0b = ifelse(is.finite(b0b),b0b,.Machine[["double.xmax"]]) 
    #bound b0a and b0b by machine precision
    
    # log_b0a = rep(-.Machine[["double.xmax"]],max_trial_size) # b0a=exp(log_b0a); log_b0a=log(b0a)
    # log_b0b = rep(-.Machine[["double.xmax"]],max_trial_size) # b0b=exp(log_b0b); log_b0b=log(b0b)
    # log_b0a[1] = log(phi[1])
    # for(k in seq(2,tnum)){
    #  log_b0b[1] = log(exp(log_b0a[1]) + phi[k])
    #  for(l in seq(2,min(k,cnum))){ #break down if only one case
    #    log_b0b[l] = log(exp(log_b0a[l]) + exp(log_b0a[l-1])*phi[k])
    #  }
    #  for(l in seq(1,min(k,cnum)))
    #    log_b0a[l] = log_b0b[l]
    # }
    #b0a=exp(log_b0a)
    #b0b=exp(log_b0b)
    
    #print(paste(bhat,cnum,tnum,snum,phi,sumphiinv,b0a,b0b))
    case_log_odds[1,j] = log(sumphiinv/(snum-cnum))
    log_bl_odds[1,j] = log(b0b[cnum-1])-log(b0b[cnum])+log(tnum - (cnum-1))-log(snum-cnum)
    baseline_odds[1,j] = exp(log_bl_odds[1,j])
    valid_trial[1,j] = fset[i,]$trial
  }
  odds_set=data.frame(t(rbind(valid_trial[1,1:j],case_log_odds[1,1:j],baseline_odds[1,1:j],log_bl_odds[1,1:j])))
  if(useTrueBeta) {
    colnames(odds_set) = c('trial','truebhat_case_log_odds','truebhat_baseline_odds','truebhat_log_bline_odds')
  } else {
    colnames(odds_set) = c('trial','case_log_odds','baseline_odds','log_bline_odds')
  }
  #coe:case_log_odds, rbe:log_bline_odds
  return(odds_set)
}
