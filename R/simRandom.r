#' Simulate exposure data
#'
#' @param disttext A string discribing what distribution to simulate
#' @return data A numeric vector of simulated data based on distribution input
#' @examples
#'
#' @export
simRandom = function(disttext,n=1) {
  
  dist = strsplit(disttext,",")[[1]]
  if(dist[1]=="norm") { data = rnorm(n, mean=as.numeric(dist[2]), sd=as.numeric(dist[3])); return(data) }
  if(dist[1]=="bern") { data = rbinom(n, size=1, prob=as.numeric(dist[2])); return(data) }
  
  stop("simDistribution not supported")
}