#' Calculate log odds
#' 
#' @param gset A dataframe containing frequency matched case control data of multiple trials. Each row is a trial 
#' @param max_trial_size Maximum number of patients per trial, presented as vi:(vi+max_trial_size*2-1) columns
#' @param npar Number of covariates
#' @param zi First index of patient's column
#' @return lo_set A dataframe containing estimated c('trial', 'varcases0','varcases1', 'varysampling','varyhat','varytilde', 'varabeta', 'varacases','varacases0', 'varahat','varatilde','varbaselineoddshat', 'varbaselineoddstilde', 'Yhat') for each trial
#' @examples
#' 
#' @export
calc_lo <- function(gset, max_trial_size, npar=1, zi=17, useTrueBeta=T){
  varcases0 = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varcases1 = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varyhat = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varytilde = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varabeta = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])  
  varacases = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varacases0 = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varahat = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varatilde = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varbaselineoddshat = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  varbaselineoddstilde = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  Yhat = matrix(rep(0,dim(gset)[1]),ncol=dim(gset)[1])
  j=0
  for(i in seq(1,dim(gset)[1])){ #iterate through all trials
    if(useTrueBeta) {
      bhat = gset[i,]$truebhat
      vbhat = gset[i,]$truevbhat
    } else {
      bhat = gset[i,]$bhat
      vbhat = gset[i,]$vbhat
    }
    cnum = gset[i,]$ncases
    tnum = gset[i,]$fmsize
    snum = gset[i,]$n
    blo = gset[i,]$baseline_odds
    zt = matrix(as.numeric(gset[i,zi:(zi+max_trial_size*npar-1)]), nrow=npar, 
                ncol=max_trial_size, byrow=FALSE)
    j=j+1
    if(cnum<3) {
      varcases0[1,j] = NA
      varcases1[1,j] = NA
      varyhat[1,j] = NA
      varytilde[1,j] = NA
      varabeta[1,j] = NA  
      varacases[1,j] = NA
      varacases0[1,j] = NA
      varahat[1,j] = NA
      varatilde[1,j] = NA
      varbaselineoddshat[1,j] = NA
      varbaselineoddstilde[1,j] = NA
      Yhat[1,j] = NA
      next
    }
    #lambda = rep(0,max_trial_size)
    #p = rep(0,max_trial_size)
    b0a = rep(0,max_trial_size)
    b0b = rep(1,max_trial_size)
    b1pa = rep(0,max_trial_size)
    b1pb = rep(0,max_trial_size)
    b1qa = rep(0,max_trial_size)
    b1qb = rep(0,max_trial_size)
    b1q2a = rep(0,max_trial_size)
    b1q2b = rep(0,max_trial_size)
    b1pqa = rep(0,max_trial_size)
    b1pqb = rep(0,max_trial_size)
    b1za = matrix(rep(0,max_trial_size*npar),nrow=npar,ncol=max_trial_size)
    b1zb = matrix(rep(0,max_trial_size*npar),nrow=npar,ncol=max_trial_size)
    b2qa = rep(0,max_trial_size)
    b2qb = rep(0,max_trial_size)
#     print(zt[1,])
#     print(blo)
#     print(bhat)
    lambda = blo*exp(zt[1,]*bhat)
    p = lambda/(1+lambda)
#     print(lambda)
#     print(p)
#     stop("stop")
    b0a[1] = lambda[1]
    b1pa[1] = lambda[1]/p[1]
    b1qa[1] = lambda[1]/(1-p[1])
    b1q2a[1] = lambda[1]/(1-p[1])**2
    b1pqa[1] = lambda[1]/(p[1]*(1-p[1]))
    b1za[1,1] = lambda[1]*zt[1,1]
    b2qa[1] = lambda[1]/(1-p[1])**2
    for(k in seq(2,tnum)){
      b0b[1] = b0a[1] + lambda[k]
      b1pb[1] = b1pa[1] + lambda[k]/p[k]
      b1qb[1] = b1qa[1] + lambda[k]/(1-p[k])
      b1q2b[1] = b1q2a[1] + lambda[k]/(1-p[k])**2
      b1pqb[1] = b1pqa[1] + lambda[k]/(p[k]*(1-p[k]))
      b1zb[1,1] = b1za[1,1] + lambda[k]*zt[1,k]
      b2qb[1] = b2qa[1] + lambda[k]/(1-p[k])**2
      for(l in seq(2,min(k,cnum))){
        b0b[l] = b0a[l] + b0a[l-1]*lambda[k]
        b1pb[l] = b1pa[l] + b1pa[l-1]*lambda[k]+ b0a[l-1]*lambda[k]/p[k]
        b1qb[l] = b1qa[l] + b1qa[l-1]*lambda[k]+ b0a[l-1]*lambda[k]/(1-p[k])
        b1q2b[l] = b1q2a[l] + b1q2a[l-1]*lambda[k]+ b0a[l-1]*lambda[k]/(1-p[k])**2
        b1pqb[l] = b1pqa[l] + b1pqa[l-1]*lambda[k]+ b0a[l-1]*lambda[k]/(p[k]*(1-p[k]))
        b1zb[1,l] = b1za[1,l] + b1za[1,l-1]*lambda[k]+ b0a[l-1]*lambda[k]*zt[1,k]
        b2qb[l] = b2qa[l] + b2qa[l-1]*lambda[k] + 2*b1qa[l-1]*lambda[k]/(1-p[k]) 
          + b0a[l-1]*lambda[k]/((1-p[k])**2)
      }
      for(l in seq(1,min(k,cnum))){
        b0a[l] = b0b[l]
        b1pa[l] = b1pb[l]
        b1qa[l] = b1qb[l]
        b1q2a[l] = b1q2b[l]
        b1pqa[l] = b1pqb[l]
        b1za[1,l] = b1zb[1,l]
        b2qa[l] = b2qb[l]
      }
    }
    sump = sum(1/p[1:tnum])                   #sum(Y^2/p), C1p
    sumq = sum(1/(1-p[1:tnum]))               #sum(Y/q), C1q, C2q
    sumpq = sum(1/(p[1:tnum]*(1-p[1:tnum])))  #sum(Y^2/pq), C1pq
    sumq2 = sum(1/(p[1:tnum])**2)             #sum(Y^2/q^2), C2q
    debug='no varytilde'
    C0 = b0b[cnum]   #C0phi = B0(|D|,R~)
    C1p = sump * b0b[cnum-1] - b1pb[cnum-1] 
    C1q = sumq * b0b[cnum-1] - b1qb[cnum-1] #C1phi = (sumq) B0(|D|-1,R~) - B1(|D|-1,R~,Y/q)
    C1pq =sumpq * b0b[cnum-1] - b1pqb[cnum-1]
    C2q = sumq*sumq*b0b[cnum-2] - 2*sumq*b1qb[cnum-2] + b2qb[cnum-2] 
      - (sumq2*b0b[cnum-2] - b1q2b[cnum-2]) 
    #C_2_phi = (sumq)^2 B_0(|D|-2,R~) - 2(sumq)B1(|D|-2,R~,Y/q) + B2(|D|-2,R~,Y/q)
    # - ( sumq^2 B0(|D|-2,R~) - B1(|D|-2,R~,Y^2/q^2) )
    crt_Yhat = C1q/C0
    #print(paste("C1q",C1q,"C0",C0,"Yhat",crt_Yhat))
    crt_varcases0 = sum((1-p[1:cnum])/(p[1:cnum]**2))
    crt_varcases1 =  C1p/C0
    varysampling = C1pq/C0 + C2q/C0 - (C1q/C0)**2
    crt_varyhat = crt_varcases0 - varysampling
    crt_varytilde = crt_varcases1 - varysampling
#     if(debug=='varytilde'){
#       print(paste("ncases",cnum,"ntot",tnum,"n",snum))
#       print(paste("b0b",str(b0b)))
#       print(paste("b1pb",str(b1pb)))
#       print(paste("sump",sump))
#       print(paste("C1p",C1p))
#       print(paste("C0",C0))
#       print(paste("crt_varcases1=C1p/C0",crt_varcases1))
#       print(paste("varysampling",varysampling))
#       stop(debug)
#     }
    lambdai = p[1:cnum]/(1-p[1:cnum])
    sum_cases_lambda = sum(1/lambdai) #lambdai=p[i]/(1-p[i])
    sum_cases_z_lambda = sum(zt[1,1:cnum]/lambdai)
    dy_alpha = -(tnum - (cnum-1)) * b0b[cnum-1]/b0b[cnum]
    dy_beta = (tnum - (cnum-1)) * (b1zb[1,cnum-1]/b0b[cnum] 
      - b0b[cnum-1]*b1zb[1,cnum]/b0b[cnum]**2)   
    crt_varabeta = vbhat * dy_beta**2
    crt_varacases = (crt_varcases1 + sum_cases_z_lambda**2*vbhat)/sum_cases_lambda**2
    crt_varacases0 = (crt_varcases0 + sum_cases_z_lambda**2*vbhat)/sum_cases_lambda**2
    crt_varahat = (crt_varyhat + crt_varabeta)/dy_alpha**2                
    crt_varatilde = (crt_varytilde + crt_varabeta)/dy_alpha**2   
    ### crt_varatilde should be always < crt_varacases
    #NOTE: stopifnot(crt_varatilde<=crt_varacases) is not always true
    crt_varbaselineoddshat = crt_varahat*blo**2
    crt_varbaselineoddstilde = crt_varatilde*blo**2
    Yhat[1,j] = crt_Yhat
    varcases0[1,j] = crt_varcases0
    varcases1[1,j] = crt_varcases1
    varyhat[1,j] = crt_varyhat
    varytilde[1,j] = crt_varytilde
    varabeta[1,j] = crt_varabeta
    varacases[1,j] = crt_varacases
    varacases0[1,j] = crt_varacases0
    varahat[1,j] = crt_varahat
    varatilde[1,j] = crt_varatilde
    varbaselineoddshat[1,j] = crt_varbaselineoddshat
    varbaselineoddstilde[1,j] = crt_varbaselineoddstilde
  }
  estimates=data.frame(unique(gset$trial), t(varcases0), t(varcases1),
                       t(varysampling), t(varyhat), t(varytilde),
                       t(varabeta), t(varacases), t(varacases0), 
                       t(varahat), t(varatilde), t(varbaselineoddshat),
                       t(varbaselineoddstilde), t(Yhat), row.names=NULL)
  if(useTrueBeta) {
    colnames(estimates)=c('trial', 'truebhat_varcases0','truebhat_varcases1',
                          'truebhat_varysampling','truebhat_varyhat','truebhat_varytilde',
                          'truebhat_varabeta', 'truebhat_varacases','truebhat_varacases0',
                          'truebhat_varahat','truebhat_varatilde','truebhat_varbaselineoddshat',
                          'truebhat_varbaselineoddstilde', 'truebhat_Yhat')
  } else {
    colnames(estimates)=c('trial', 'varcases0','varcases1',
                        'varysampling','varyhat','varytilde',
                        'varabeta', 'varacases','varacases0',
                        'varahat','varatilde','varbaselineoddshat',
                        'varbaselineoddstilde', 'Yhat')
  }
#   print(varcases0)
#   print(varcases1)
#   print(varysampling)
#   print(varyhat)
#   print(varytilde)
#   print(varabeta)
#   print(varacases)
#   print(varacases0)
#   print(varahat)
#   print(varatilde)
#   print(varbaselineoddshat)
#   print(varbaselineoddstilde)
  lo_set = estimates
  return(lo_set)
}
