#' Calculate survival estimate
#'
#' @param fm A dataframe containing frequency matched data
#' @param oddsRatio A numeric for odds ratio
#' @param useTrueBeta A boolean to indicate whether true estiamted baseline odds should be used in computation
#' @return betas_data A data of log odds estimates based on survival
#' @examples
#'
#' @export
survivalEstimate2 = function(fm, oddsRatio, trial_size, case_number, useTrueBeta=F) {
  betas_data=rep(0,5*length(unique(fm$trial)))
  dim(betas_data)=c(length(unique(fm$trial)),5)
  j=0
  for(ti in unique(fm$trial)){ #NOTE: fm is case_cont in original coding?
    j=j+1
    fmsubset = fm[fm$trial==ti,]   #only use data from ti-th trial
    clogitfit = tryCatch(survival::clogit(d ~ z, data=fmsubset), 
                         error=function(e) { 
                           message("clogit failed with error:", str(e))
                           return(NULL)
                         }
    )
    if(useTrueBeta) {
      bhat=log(oddsRatio) #this is true beta
      vbhat=0
    } else {
      bhat=ifelse(is.null(clogitfit), NA, as.vector(clogitfit$coefficients)) #this is estimated beta, could be NA if clogit failed
      vbhat=ifelse(is.null(clogitfit), NA, diag(clogitfit$var))
    }
    betas_data[j,]=c(ti,bhat,vbhat,trial_size[j],case_number[j])
  }
  return(betas_data)
}

# survivalEstimate=function(fm, oddsRatio, useTrueBeta=F) {
#   betas_data=rep(0,5*length(unique(fm$trial)))
#   dim(betas_data)=c(length(unique(fm$trial)),5)
#   j=0
#   for(ti in unique(fm$trial)){ #NOTE: fm is case_cont in original coding?
#     j=j+1
#     fmsubset = fm[fm$trial==ti,]   #only use data from ti-th trial
#     coxphfit = tryCatch(survival::coxph(survival::Surv(d, exit, d) ~ z, data=fmsubset), 
#       warning=function(w) { 
#           print(str(w))
#           return(NULL) 
#       }
#     )
#     if(is.null(coxphfit)) {
#       betas_data[j,]=c(ti,NA,NA,trial_size[j],case_number[j])
#       if(useTrueBeta) { betas_data[j,2] = bhat; betas_data[j,3] = vbhat; } #return true betas even if cox failed
#     } else {
#       if(useTrueBeta) {
#         bhat=log(oddsRatio) #this is true beta
#         vbhat=0
#       } else {
#         bhat=as.vector(coxphfit$coefficients) #this is estimated beta
#         vbhat=diag(coxphfit$var)
#       }
#       betas_data[j,]=c(ti,bhat,vbhat,trial_size[j],case_number[j])
#     }
#   }
#   return(betas_data)
# } #this estimate is not very accurate